import 'package:fpdart/fpdart.dart';
import 'package:majootestcase/models/input/value_failures.dart';

class ValueValidators {
  static Either<ValueFailure<String>, String> validateMaxStringLength(
    String input,
    int maxLength,
  ) {
    if (input.length <= maxLength) {
      return right(input);
    } else {
      return left(
        ValueFailure.exceedingLength(failedValue: input, max: maxLength),
      );
    }
  }

  static Either<ValueFailure<String>, String> validateMinStringLength(
    String input,
    int minLength,
  ) {
    if (input.length >= minLength) {
      return right(input);
    } else {
      return left(
        ValueFailure.lengthTooShort(failedValue: input, min: minLength),
      );
    }
  }

  static Either<ValueFailure<String>, String> validateStringNotEmpty(
      String input) {
    if (input.isNotEmpty) {
      return right(input);
    } else {
      return left(ValueFailure.empty(failedValue: input));
    }
  }

  static Either<ValueFailure<String>, String> validateEmail(String input) {
    if (input.isEmpty) {
      return left(ValueFailure.empty(failedValue: input));
    }
    if (RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+")
        .hasMatch(input)) {
      return right(input);
    } else {
      return left(ValueFailure.invalidEmail(failedValue: input));
    }
  }

  static Either<ValueFailure<String>, String> validateToken(
      String input, int maxLength) {
    if (input.length == maxLength) {
      return right(input);
    } else if (input.length < maxLength) {
      return left(ValueFailure.shortToken(failedValue: input));
    } else {
      return left(
          ValueFailure.exceedingLength(failedValue: input, max: maxLength));
    }
  }

  static Either<ValueFailure<String>, String> validatePassword(String input) {
    final RegExp _passwordRegExp = RegExp(
      r'^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,64})$',
    );
    if (input.isEmpty) {
      return left(ValueFailure.empty(failedValue: input));
    }
    if (input.length <= 6 && _passwordRegExp.hasMatch(input)) {
      return right(input);
    } else {
      return left(ValueFailure.weakPassword(failedValue: input));
    }
  }
}
