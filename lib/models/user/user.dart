class User {
  String email;
  String userName;
  String password;

  User({
    required this.email,
    required this.userName,
    required this.password,
  });

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': userName};
}
