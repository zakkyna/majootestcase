import 'package:fpdart/fpdart.dart';
import 'package:majootestcase/models/input/value_failures.dart';
import 'package:majootestcase/models/input/value_objects.dart';
import 'package:majootestcase/models/input/value_validator.dart';

class Password extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory Password(
    String input,
  ) {
    return Password._(ValueValidators.validateStringNotEmpty(input));
  }

  const Password._(this.value);
}

class EmailAddress extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory EmailAddress(String input) {
    return EmailAddress._(
      ValueValidators.validateEmail(input),
    );
  }

  const EmailAddress._(this.value);
}

class UserName extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory UserName(String input) {
    return UserName._(
      ValueValidators.validateStringNotEmpty(input),
    );
  }

  const UserName._(this.value);
}
