import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_failure.freezed.dart';

@freezed
class AuthFailure with _$AuthFailure {
  const factory AuthFailure.cancelledByUser() = _CancelledByUser;
  const factory AuthFailure.serverError({
    required String message,
    required int code,
  }) = _ServerError;
  const factory AuthFailure.noInternet() = _NoInternet;
  const factory AuthFailure.timeOut() = _TimeOut;
  const factory AuthFailure.unexpected() = _Unexpected;
  const factory AuthFailure.userNotFound() = _UserNotFound;
  const factory AuthFailure.emailAlreadyInUse() = _EmailAlreadyInUse;
  const factory AuthFailure.invalidEmailAndPasswordCombination() =
      _InvalidEmailAndPasswordCombination;

  const AuthFailure._();
  String mapErrorMessage() {
    return map(
      cancelledByUser: (_) => 'Dibatalkan',
      serverError: (e) => e.message,
      invalidEmailAndPasswordCombination: (_) =>
          'Kombinasi email dan password tidak valid',
      noInternet: (_) => 'Tidak ada koneksi internet',
      emailAlreadyInUse: (_) => 'Email sudah terdaftar',
      unexpected: (_) => 'Terjadi kesalahan',
      userNotFound: (_) => 'User tidak ditemukan',
      timeOut: (_) => 'Waktu koneksi habis',
    );
  }
}
