import 'package:freezed_annotation/freezed_annotation.dart';
part 'movie_failure.freezed.dart';

@freezed
class MovieFailure with _$MovieFailure {
  const factory MovieFailure.noInternet() = _NoInternet;
  const factory MovieFailure.timeOut() = _TimeOut;
  const factory MovieFailure.serverError() = _ServerError;
  const factory MovieFailure.unexpected() = _Unexpected;
  const factory MovieFailure.failedParseJson() = _FailedParseJson;
}
