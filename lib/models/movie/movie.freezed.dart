// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return _Movie.fromJson(json);
}

/// @nodoc
class _$MovieTearOff {
  const _$MovieTearOff();

  _Movie call(
      {@JsonKey(includeIfNull: true)
          required bool? video,
      required int id,
      required String overview,
      @JsonKey(includeIfNull: true)
          required String? releaseDate,
      @JsonKey(includeIfNull: true)
          required int? voteCount,
      @JsonKey(includeIfNull: true)
          required bool? adult,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String backdropPath,
      @JsonKey(includeIfNull: true, defaultValue: 0)
          required double voteAverage,
      required List<int> genreIds,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String title,
      required String originalLanguage,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String originalTitle,
      required String posterPath,
      required double popularity,
      required String mediaType,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String originalName,
      @JsonKey(includeIfNull: true)
          required List<String>? originCountry,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String name,
      @JsonKey(includeIfNull: true)
          required String? firstAirDate}) {
    return _Movie(
      video: video,
      id: id,
      overview: overview,
      releaseDate: releaseDate,
      voteCount: voteCount,
      adult: adult,
      backdropPath: backdropPath,
      voteAverage: voteAverage,
      genreIds: genreIds,
      title: title,
      originalLanguage: originalLanguage,
      originalTitle: originalTitle,
      posterPath: posterPath,
      popularity: popularity,
      mediaType: mediaType,
      originalName: originalName,
      originCountry: originCountry,
      name: name,
      firstAirDate: firstAirDate,
    );
  }

  Movie fromJson(Map<String, Object?> json) {
    return Movie.fromJson(json);
  }
}

/// @nodoc
const $Movie = _$MovieTearOff();

/// @nodoc
mixin _$Movie {
  @JsonKey(includeIfNull: true)
  bool? get video => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String get overview => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true)
  String? get releaseDate => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true)
  int? get voteCount => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true)
  bool? get adult => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get backdropPath => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: 0)
  double get voteAverage => throw _privateConstructorUsedError;
  List<int> get genreIds => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get title => throw _privateConstructorUsedError;
  String get originalLanguage => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get originalTitle => throw _privateConstructorUsedError;
  String get posterPath => throw _privateConstructorUsedError;
  double get popularity => throw _privateConstructorUsedError;
  String get mediaType => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get originalName => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true)
  List<String>? get originCountry => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get name => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: true)
  String? get firstAirDate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieCopyWith<Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(includeIfNull: true) bool? video,
      int id,
      String overview,
      @JsonKey(includeIfNull: true) String? releaseDate,
      @JsonKey(includeIfNull: true) int? voteCount,
      @JsonKey(includeIfNull: true) bool? adult,
      @JsonKey(includeIfNull: true, defaultValue: '') String backdropPath,
      @JsonKey(includeIfNull: true, defaultValue: 0) double voteAverage,
      List<int> genreIds,
      @JsonKey(includeIfNull: true, defaultValue: '') String title,
      String originalLanguage,
      @JsonKey(includeIfNull: true, defaultValue: '') String originalTitle,
      String posterPath,
      double popularity,
      String mediaType,
      @JsonKey(includeIfNull: true, defaultValue: '') String originalName,
      @JsonKey(includeIfNull: true) List<String>? originCountry,
      @JsonKey(includeIfNull: true, defaultValue: '') String name,
      @JsonKey(includeIfNull: true) String? firstAirDate});
}

/// @nodoc
class _$MovieCopyWithImpl<$Res> implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  final Movie _value;
  // ignore: unused_field
  final $Res Function(Movie) _then;

  @override
  $Res call({
    Object? video = freezed,
    Object? id = freezed,
    Object? overview = freezed,
    Object? releaseDate = freezed,
    Object? voteCount = freezed,
    Object? adult = freezed,
    Object? backdropPath = freezed,
    Object? voteAverage = freezed,
    Object? genreIds = freezed,
    Object? title = freezed,
    Object? originalLanguage = freezed,
    Object? originalTitle = freezed,
    Object? posterPath = freezed,
    Object? popularity = freezed,
    Object? mediaType = freezed,
    Object? originalName = freezed,
    Object? originCountry = freezed,
    Object? name = freezed,
    Object? firstAirDate = freezed,
  }) {
    return _then(_value.copyWith(
      video: video == freezed
          ? _value.video
          : video // ignore: cast_nullable_to_non_nullable
              as bool?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String?,
      voteCount: voteCount == freezed
          ? _value.voteCount
          : voteCount // ignore: cast_nullable_to_non_nullable
              as int?,
      adult: adult == freezed
          ? _value.adult
          : adult // ignore: cast_nullable_to_non_nullable
              as bool?,
      backdropPath: backdropPath == freezed
          ? _value.backdropPath
          : backdropPath // ignore: cast_nullable_to_non_nullable
              as String,
      voteAverage: voteAverage == freezed
          ? _value.voteAverage
          : voteAverage // ignore: cast_nullable_to_non_nullable
              as double,
      genreIds: genreIds == freezed
          ? _value.genreIds
          : genreIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      originalLanguage: originalLanguage == freezed
          ? _value.originalLanguage
          : originalLanguage // ignore: cast_nullable_to_non_nullable
              as String,
      originalTitle: originalTitle == freezed
          ? _value.originalTitle
          : originalTitle // ignore: cast_nullable_to_non_nullable
              as String,
      posterPath: posterPath == freezed
          ? _value.posterPath
          : posterPath // ignore: cast_nullable_to_non_nullable
              as String,
      popularity: popularity == freezed
          ? _value.popularity
          : popularity // ignore: cast_nullable_to_non_nullable
              as double,
      mediaType: mediaType == freezed
          ? _value.mediaType
          : mediaType // ignore: cast_nullable_to_non_nullable
              as String,
      originalName: originalName == freezed
          ? _value.originalName
          : originalName // ignore: cast_nullable_to_non_nullable
              as String,
      originCountry: originCountry == freezed
          ? _value.originCountry
          : originCountry // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      firstAirDate: firstAirDate == freezed
          ? _value.firstAirDate
          : firstAirDate // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$MovieCopyWith(_Movie value, $Res Function(_Movie) then) =
      __$MovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(includeIfNull: true) bool? video,
      int id,
      String overview,
      @JsonKey(includeIfNull: true) String? releaseDate,
      @JsonKey(includeIfNull: true) int? voteCount,
      @JsonKey(includeIfNull: true) bool? adult,
      @JsonKey(includeIfNull: true, defaultValue: '') String backdropPath,
      @JsonKey(includeIfNull: true, defaultValue: 0) double voteAverage,
      List<int> genreIds,
      @JsonKey(includeIfNull: true, defaultValue: '') String title,
      String originalLanguage,
      @JsonKey(includeIfNull: true, defaultValue: '') String originalTitle,
      String posterPath,
      double popularity,
      String mediaType,
      @JsonKey(includeIfNull: true, defaultValue: '') String originalName,
      @JsonKey(includeIfNull: true) List<String>? originCountry,
      @JsonKey(includeIfNull: true, defaultValue: '') String name,
      @JsonKey(includeIfNull: true) String? firstAirDate});
}

/// @nodoc
class __$MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res>
    implements _$MovieCopyWith<$Res> {
  __$MovieCopyWithImpl(_Movie _value, $Res Function(_Movie) _then)
      : super(_value, (v) => _then(v as _Movie));

  @override
  _Movie get _value => super._value as _Movie;

  @override
  $Res call({
    Object? video = freezed,
    Object? id = freezed,
    Object? overview = freezed,
    Object? releaseDate = freezed,
    Object? voteCount = freezed,
    Object? adult = freezed,
    Object? backdropPath = freezed,
    Object? voteAverage = freezed,
    Object? genreIds = freezed,
    Object? title = freezed,
    Object? originalLanguage = freezed,
    Object? originalTitle = freezed,
    Object? posterPath = freezed,
    Object? popularity = freezed,
    Object? mediaType = freezed,
    Object? originalName = freezed,
    Object? originCountry = freezed,
    Object? name = freezed,
    Object? firstAirDate = freezed,
  }) {
    return _then(_Movie(
      video: video == freezed
          ? _value.video
          : video // ignore: cast_nullable_to_non_nullable
              as bool?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String?,
      voteCount: voteCount == freezed
          ? _value.voteCount
          : voteCount // ignore: cast_nullable_to_non_nullable
              as int?,
      adult: adult == freezed
          ? _value.adult
          : adult // ignore: cast_nullable_to_non_nullable
              as bool?,
      backdropPath: backdropPath == freezed
          ? _value.backdropPath
          : backdropPath // ignore: cast_nullable_to_non_nullable
              as String,
      voteAverage: voteAverage == freezed
          ? _value.voteAverage
          : voteAverage // ignore: cast_nullable_to_non_nullable
              as double,
      genreIds: genreIds == freezed
          ? _value.genreIds
          : genreIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      originalLanguage: originalLanguage == freezed
          ? _value.originalLanguage
          : originalLanguage // ignore: cast_nullable_to_non_nullable
              as String,
      originalTitle: originalTitle == freezed
          ? _value.originalTitle
          : originalTitle // ignore: cast_nullable_to_non_nullable
              as String,
      posterPath: posterPath == freezed
          ? _value.posterPath
          : posterPath // ignore: cast_nullable_to_non_nullable
              as String,
      popularity: popularity == freezed
          ? _value.popularity
          : popularity // ignore: cast_nullable_to_non_nullable
              as double,
      mediaType: mediaType == freezed
          ? _value.mediaType
          : mediaType // ignore: cast_nullable_to_non_nullable
              as String,
      originalName: originalName == freezed
          ? _value.originalName
          : originalName // ignore: cast_nullable_to_non_nullable
              as String,
      originCountry: originCountry == freezed
          ? _value.originCountry
          : originCountry // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      firstAirDate: firstAirDate == freezed
          ? _value.firstAirDate
          : firstAirDate // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake)
class _$_Movie implements _Movie {
  const _$_Movie(
      {@JsonKey(includeIfNull: true)
          required this.video,
      required this.id,
      required this.overview,
      @JsonKey(includeIfNull: true)
          required this.releaseDate,
      @JsonKey(includeIfNull: true)
          required this.voteCount,
      @JsonKey(includeIfNull: true)
          required this.adult,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required this.backdropPath,
      @JsonKey(includeIfNull: true, defaultValue: 0)
          required this.voteAverage,
      required this.genreIds,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required this.title,
      required this.originalLanguage,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required this.originalTitle,
      required this.posterPath,
      required this.popularity,
      required this.mediaType,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required this.originalName,
      @JsonKey(includeIfNull: true)
          required this.originCountry,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required this.name,
      @JsonKey(includeIfNull: true)
          required this.firstAirDate});

  factory _$_Movie.fromJson(Map<String, dynamic> json) =>
      _$$_MovieFromJson(json);

  @override
  @JsonKey(includeIfNull: true)
  final bool? video;
  @override
  final int id;
  @override
  final String overview;
  @override
  @JsonKey(includeIfNull: true)
  final String? releaseDate;
  @override
  @JsonKey(includeIfNull: true)
  final int? voteCount;
  @override
  @JsonKey(includeIfNull: true)
  final bool? adult;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  final String backdropPath;
  @override
  @JsonKey(includeIfNull: true, defaultValue: 0)
  final double voteAverage;
  @override
  final List<int> genreIds;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  final String title;
  @override
  final String originalLanguage;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  final String originalTitle;
  @override
  final String posterPath;
  @override
  final double popularity;
  @override
  final String mediaType;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  final String originalName;
  @override
  @JsonKey(includeIfNull: true)
  final List<String>? originCountry;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  final String name;
  @override
  @JsonKey(includeIfNull: true)
  final String? firstAirDate;

  @override
  String toString() {
    return 'Movie(video: $video, id: $id, overview: $overview, releaseDate: $releaseDate, voteCount: $voteCount, adult: $adult, backdropPath: $backdropPath, voteAverage: $voteAverage, genreIds: $genreIds, title: $title, originalLanguage: $originalLanguage, originalTitle: $originalTitle, posterPath: $posterPath, popularity: $popularity, mediaType: $mediaType, originalName: $originalName, originCountry: $originCountry, name: $name, firstAirDate: $firstAirDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Movie &&
            const DeepCollectionEquality().equals(other.video, video) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.overview, overview) &&
            const DeepCollectionEquality()
                .equals(other.releaseDate, releaseDate) &&
            const DeepCollectionEquality().equals(other.voteCount, voteCount) &&
            const DeepCollectionEquality().equals(other.adult, adult) &&
            const DeepCollectionEquality()
                .equals(other.backdropPath, backdropPath) &&
            const DeepCollectionEquality()
                .equals(other.voteAverage, voteAverage) &&
            const DeepCollectionEquality().equals(other.genreIds, genreIds) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality()
                .equals(other.originalLanguage, originalLanguage) &&
            const DeepCollectionEquality()
                .equals(other.originalTitle, originalTitle) &&
            const DeepCollectionEquality()
                .equals(other.posterPath, posterPath) &&
            const DeepCollectionEquality()
                .equals(other.popularity, popularity) &&
            const DeepCollectionEquality().equals(other.mediaType, mediaType) &&
            const DeepCollectionEquality()
                .equals(other.originalName, originalName) &&
            const DeepCollectionEquality()
                .equals(other.originCountry, originCountry) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality()
                .equals(other.firstAirDate, firstAirDate));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(video),
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(overview),
        const DeepCollectionEquality().hash(releaseDate),
        const DeepCollectionEquality().hash(voteCount),
        const DeepCollectionEquality().hash(adult),
        const DeepCollectionEquality().hash(backdropPath),
        const DeepCollectionEquality().hash(voteAverage),
        const DeepCollectionEquality().hash(genreIds),
        const DeepCollectionEquality().hash(title),
        const DeepCollectionEquality().hash(originalLanguage),
        const DeepCollectionEquality().hash(originalTitle),
        const DeepCollectionEquality().hash(posterPath),
        const DeepCollectionEquality().hash(popularity),
        const DeepCollectionEquality().hash(mediaType),
        const DeepCollectionEquality().hash(originalName),
        const DeepCollectionEquality().hash(originCountry),
        const DeepCollectionEquality().hash(name),
        const DeepCollectionEquality().hash(firstAirDate)
      ]);

  @JsonKey(ignore: true)
  @override
  _$MovieCopyWith<_Movie> get copyWith =>
      __$MovieCopyWithImpl<_Movie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieToJson(this);
  }
}

abstract class _Movie implements Movie {
  const factory _Movie(
      {@JsonKey(includeIfNull: true)
          required bool? video,
      required int id,
      required String overview,
      @JsonKey(includeIfNull: true)
          required String? releaseDate,
      @JsonKey(includeIfNull: true)
          required int? voteCount,
      @JsonKey(includeIfNull: true)
          required bool? adult,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String backdropPath,
      @JsonKey(includeIfNull: true, defaultValue: 0)
          required double voteAverage,
      required List<int> genreIds,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String title,
      required String originalLanguage,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String originalTitle,
      required String posterPath,
      required double popularity,
      required String mediaType,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String originalName,
      @JsonKey(includeIfNull: true)
          required List<String>? originCountry,
      @JsonKey(includeIfNull: true, defaultValue: '')
          required String name,
      @JsonKey(includeIfNull: true)
          required String? firstAirDate}) = _$_Movie;

  factory _Movie.fromJson(Map<String, dynamic> json) = _$_Movie.fromJson;

  @override
  @JsonKey(includeIfNull: true)
  bool? get video;
  @override
  int get id;
  @override
  String get overview;
  @override
  @JsonKey(includeIfNull: true)
  String? get releaseDate;
  @override
  @JsonKey(includeIfNull: true)
  int? get voteCount;
  @override
  @JsonKey(includeIfNull: true)
  bool? get adult;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get backdropPath;
  @override
  @JsonKey(includeIfNull: true, defaultValue: 0)
  double get voteAverage;
  @override
  List<int> get genreIds;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get title;
  @override
  String get originalLanguage;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get originalTitle;
  @override
  String get posterPath;
  @override
  double get popularity;
  @override
  String get mediaType;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get originalName;
  @override
  @JsonKey(includeIfNull: true)
  List<String>? get originCountry;
  @override
  @JsonKey(includeIfNull: true, defaultValue: '')
  String get name;
  @override
  @JsonKey(includeIfNull: true)
  String? get firstAirDate;
  @override
  @JsonKey(ignore: true)
  _$MovieCopyWith<_Movie> get copyWith => throw _privateConstructorUsedError;
}
