// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Movie _$$_MovieFromJson(Map<String, dynamic> json) => _$_Movie(
      video: json['video'] as bool?,
      id: json['id'] as int,
      overview: json['overview'] as String,
      releaseDate: json['release_date'] as String?,
      voteCount: json['vote_count'] as int?,
      adult: json['adult'] as bool?,
      backdropPath: json['backdrop_path'] as String? ?? '',
      voteAverage: (json['vote_average'] as num?)?.toDouble() ?? 0,
      genreIds:
          (json['genre_ids'] as List<dynamic>).map((e) => e as int).toList(),
      title: json['title'] as String? ?? '',
      originalLanguage: json['original_language'] as String,
      originalTitle: json['original_title'] as String? ?? '',
      posterPath: json['poster_path'] as String,
      popularity: (json['popularity'] as num).toDouble(),
      mediaType: json['media_type'] as String,
      originalName: json['original_name'] as String? ?? '',
      originCountry: (json['origin_country'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      name: json['name'] as String? ?? '',
      firstAirDate: json['first_air_date'] as String?,
    );

Map<String, dynamic> _$$_MovieToJson(_$_Movie instance) => <String, dynamic>{
      'video': instance.video,
      'id': instance.id,
      'overview': instance.overview,
      'release_date': instance.releaseDate,
      'vote_count': instance.voteCount,
      'adult': instance.adult,
      'backdrop_path': instance.backdropPath,
      'vote_average': instance.voteAverage,
      'genre_ids': instance.genreIds,
      'title': instance.title,
      'original_language': instance.originalLanguage,
      'original_title': instance.originalTitle,
      'poster_path': instance.posterPath,
      'popularity': instance.popularity,
      'media_type': instance.mediaType,
      'original_name': instance.originalName,
      'origin_country': instance.originCountry,
      'name': instance.name,
      'first_air_date': instance.firstAirDate,
    };
