// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'movie.freezed.dart';
part 'movie.g.dart';

@freezed
abstract class Movie with _$Movie {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Movie({
    @JsonKey(includeIfNull: true) required bool? video,
    required int id,
    required String overview,
    @JsonKey(includeIfNull: true) required String? releaseDate,
    @JsonKey(includeIfNull: true) required int? voteCount,
    @JsonKey(includeIfNull: true) required bool? adult,
    @JsonKey(includeIfNull: true, defaultValue: '')
        required String backdropPath,
    @JsonKey(includeIfNull: true, defaultValue: 0) required double voteAverage,
    required List<int> genreIds,
    @JsonKey(includeIfNull: true, defaultValue: '') required String title,
    required String originalLanguage,
    @JsonKey(includeIfNull: true, defaultValue: '')
        required String originalTitle,
    required String posterPath,
    required double popularity,
    required String mediaType,
    @JsonKey(includeIfNull: true, defaultValue: '')
        required String originalName,
    @JsonKey(includeIfNull: true) required List<String>? originCountry,
    @JsonKey(includeIfNull: true, defaultValue: '') required String name,
    @JsonKey(includeIfNull: true) required String? firstAirDate,
  }) = _Movie;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
}
