import 'package:bloc/bloc.dart';
import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/failures/movie/movie_failure.dart';
import 'package:majootestcase/models/movie/movie_response.dart';
import 'package:majootestcase/repository/movie/movie_repository.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

@lazySingleton
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final MovieRepository _movieRepository;
  HomeBloc(this._movieRepository) : super(HomeState.initial()) {
    on<HomeEvent>((event, emit) async {
      await event.map(
        getMovies: (_event) async {
          final failureOrSuccess = await _movieRepository.getMovies();
          emit(
            state.copyWith(
              moviesFailureOrSuccessOption: optionOf(failureOrSuccess),
            ),
          );
        },
      );
    });
  }
}
