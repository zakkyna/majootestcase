part of 'home_bloc.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState({
    required Option<Either<MovieFailure, MovieResponse>>
        moviesFailureOrSuccessOption,
  }) = _HomeState;

  factory HomeState.initial() => HomeState(
        moviesFailureOrSuccessOption: none(),
      );
}
