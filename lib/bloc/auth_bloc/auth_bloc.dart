import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/repository/auth/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';
part 'auth_bloc.freezed.dart';

@lazySingleton
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;
  AuthBloc(this._authRepository) : super(_Initial()) {
    on<AuthEvent>((event, emit) async {
      await event.map(
        authCheck: (_event) async {
          emit(AuthState.loading());
          final user = await _authRepository.getSignedInUser();
          user.match(
            (t) => emit(AuthState.authenticated()),
            () => emit(
              AuthState.unauthenticated(),
            ),
          );
        },
        signOut: (_event) async {
          await _authRepository.signOut();
          emit(AuthState.unauthenticated());
        },
      );
    });
  }
}
