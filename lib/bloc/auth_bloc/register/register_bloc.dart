import 'package:bloc/bloc.dart';
import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/auth/auth_value_objects.dart';
import 'package:majootestcase/models/failures/auth/auth_failure.dart';
import 'package:majootestcase/models/user/user.dart';
import 'package:majootestcase/repository/auth/auth_repository.dart';

part 'register_event.dart';
part 'register_state.dart';
part 'register_bloc.freezed.dart';

@lazySingleton
class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository _authRepository;
  RegisterBloc(this._authRepository) : super(RegisterState.initial()) {
    on<RegisterEvent>((event, emit) async {
      await event.map(
        userNameChanged: (e) async {
          emit(
            state.copyWith(
              userName: UserName(e.userNameStr),
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        emailChanged: (e) async {
          emit(
            state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        passwordChanged: (e) async {
          emit(
            state.copyWith(
              password: Password(e.passwordStr),
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        toggleShowPasswordPressed: (e) async {
          emit(
            state.copyWith(
              showPassword: !state.showPassword,
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        registerPressed: (e) async {
          Either<AuthFailure, User>? failureOrSuccess;

          final isNameValid = state.userName.isValid();
          final isEmailValid = state.emailAddress.isValid();
          final isPasswordValid = state.password.isValid();
          if (isEmailValid && isPasswordValid && isNameValid) {
            emit(
              state.copyWith(
                isSubmitting: true,
                authFailureOrSuccessOption: none(),
              ),
            );

            failureOrSuccess =
                await _authRepository.registerWithEmailAndPassword(
              userName: state.userName,
              emailAddress: state.emailAddress,
              password: state.password,
            );
          }
          emit(
            state.copyWith(
              isSubmitting: false,
              showErrorMessages: true,
              authFailureOrSuccessOption: optionOf(failureOrSuccess),
            ),
          );
        },
        resetError: (_event) async {
          emit(
            state.copyWith(
              showErrorMessages: false,
            ),
          );
        },
        resetAll: (_event) async {
          emit(RegisterState.initial());
        },
      );
    });
  }
}
