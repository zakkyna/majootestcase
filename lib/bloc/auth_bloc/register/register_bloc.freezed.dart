// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'register_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RegisterEventTearOff {
  const _$RegisterEventTearOff();

  _UserNameChanged userNameChanged(String userNameStr) {
    return _UserNameChanged(
      userNameStr,
    );
  }

  _EmailChanged emailChanged(String emailStr) {
    return _EmailChanged(
      emailStr,
    );
  }

  _PasswordChanged passwordChanged(String passwordStr) {
    return _PasswordChanged(
      passwordStr,
    );
  }

  _ToggleShowPasswordPressed toggleShowPasswordPressed() {
    return const _ToggleShowPasswordPressed();
  }

  _RegisterPressed registerPressed() {
    return const _RegisterPressed();
  }

  _ResetError resetError() {
    return const _ResetError();
  }

  _ResetAll resetAll() {
    return const _ResetAll();
  }
}

/// @nodoc
const $RegisterEvent = _$RegisterEventTearOff();

/// @nodoc
mixin _$RegisterEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterEventCopyWith<$Res> {
  factory $RegisterEventCopyWith(
          RegisterEvent value, $Res Function(RegisterEvent) then) =
      _$RegisterEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$RegisterEventCopyWithImpl<$Res>
    implements $RegisterEventCopyWith<$Res> {
  _$RegisterEventCopyWithImpl(this._value, this._then);

  final RegisterEvent _value;
  // ignore: unused_field
  final $Res Function(RegisterEvent) _then;
}

/// @nodoc
abstract class _$UserNameChangedCopyWith<$Res> {
  factory _$UserNameChangedCopyWith(
          _UserNameChanged value, $Res Function(_UserNameChanged) then) =
      __$UserNameChangedCopyWithImpl<$Res>;
  $Res call({String userNameStr});
}

/// @nodoc
class __$UserNameChangedCopyWithImpl<$Res>
    extends _$RegisterEventCopyWithImpl<$Res>
    implements _$UserNameChangedCopyWith<$Res> {
  __$UserNameChangedCopyWithImpl(
      _UserNameChanged _value, $Res Function(_UserNameChanged) _then)
      : super(_value, (v) => _then(v as _UserNameChanged));

  @override
  _UserNameChanged get _value => super._value as _UserNameChanged;

  @override
  $Res call({
    Object? userNameStr = freezed,
  }) {
    return _then(_UserNameChanged(
      userNameStr == freezed
          ? _value.userNameStr
          : userNameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_UserNameChanged implements _UserNameChanged {
  const _$_UserNameChanged(this.userNameStr);

  @override
  final String userNameStr;

  @override
  String toString() {
    return 'RegisterEvent.userNameChanged(userNameStr: $userNameStr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _UserNameChanged &&
            const DeepCollectionEquality()
                .equals(other.userNameStr, userNameStr));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(userNameStr));

  @JsonKey(ignore: true)
  @override
  _$UserNameChangedCopyWith<_UserNameChanged> get copyWith =>
      __$UserNameChangedCopyWithImpl<_UserNameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return userNameChanged(userNameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return userNameChanged?.call(userNameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (userNameChanged != null) {
      return userNameChanged(userNameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return userNameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return userNameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (userNameChanged != null) {
      return userNameChanged(this);
    }
    return orElse();
  }
}

abstract class _UserNameChanged implements RegisterEvent {
  const factory _UserNameChanged(String userNameStr) = _$_UserNameChanged;

  String get userNameStr;
  @JsonKey(ignore: true)
  _$UserNameChangedCopyWith<_UserNameChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$EmailChangedCopyWith<$Res> {
  factory _$EmailChangedCopyWith(
          _EmailChanged value, $Res Function(_EmailChanged) then) =
      __$EmailChangedCopyWithImpl<$Res>;
  $Res call({String emailStr});
}

/// @nodoc
class __$EmailChangedCopyWithImpl<$Res>
    extends _$RegisterEventCopyWithImpl<$Res>
    implements _$EmailChangedCopyWith<$Res> {
  __$EmailChangedCopyWithImpl(
      _EmailChanged _value, $Res Function(_EmailChanged) _then)
      : super(_value, (v) => _then(v as _EmailChanged));

  @override
  _EmailChanged get _value => super._value as _EmailChanged;

  @override
  $Res call({
    Object? emailStr = freezed,
  }) {
    return _then(_EmailChanged(
      emailStr == freezed
          ? _value.emailStr
          : emailStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_EmailChanged implements _EmailChanged {
  const _$_EmailChanged(this.emailStr);

  @override
  final String emailStr;

  @override
  String toString() {
    return 'RegisterEvent.emailChanged(emailStr: $emailStr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _EmailChanged &&
            const DeepCollectionEquality().equals(other.emailStr, emailStr));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(emailStr));

  @JsonKey(ignore: true)
  @override
  _$EmailChangedCopyWith<_EmailChanged> get copyWith =>
      __$EmailChangedCopyWithImpl<_EmailChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return emailChanged(emailStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return emailChanged?.call(emailStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(emailStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return emailChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return emailChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(this);
    }
    return orElse();
  }
}

abstract class _EmailChanged implements RegisterEvent {
  const factory _EmailChanged(String emailStr) = _$_EmailChanged;

  String get emailStr;
  @JsonKey(ignore: true)
  _$EmailChangedCopyWith<_EmailChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$PasswordChangedCopyWith<$Res> {
  factory _$PasswordChangedCopyWith(
          _PasswordChanged value, $Res Function(_PasswordChanged) then) =
      __$PasswordChangedCopyWithImpl<$Res>;
  $Res call({String passwordStr});
}

/// @nodoc
class __$PasswordChangedCopyWithImpl<$Res>
    extends _$RegisterEventCopyWithImpl<$Res>
    implements _$PasswordChangedCopyWith<$Res> {
  __$PasswordChangedCopyWithImpl(
      _PasswordChanged _value, $Res Function(_PasswordChanged) _then)
      : super(_value, (v) => _then(v as _PasswordChanged));

  @override
  _PasswordChanged get _value => super._value as _PasswordChanged;

  @override
  $Res call({
    Object? passwordStr = freezed,
  }) {
    return _then(_PasswordChanged(
      passwordStr == freezed
          ? _value.passwordStr
          : passwordStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PasswordChanged implements _PasswordChanged {
  const _$_PasswordChanged(this.passwordStr);

  @override
  final String passwordStr;

  @override
  String toString() {
    return 'RegisterEvent.passwordChanged(passwordStr: $passwordStr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PasswordChanged &&
            const DeepCollectionEquality()
                .equals(other.passwordStr, passwordStr));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(passwordStr));

  @JsonKey(ignore: true)
  @override
  _$PasswordChangedCopyWith<_PasswordChanged> get copyWith =>
      __$PasswordChangedCopyWithImpl<_PasswordChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return passwordChanged(passwordStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return passwordChanged?.call(passwordStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (passwordChanged != null) {
      return passwordChanged(passwordStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return passwordChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return passwordChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (passwordChanged != null) {
      return passwordChanged(this);
    }
    return orElse();
  }
}

abstract class _PasswordChanged implements RegisterEvent {
  const factory _PasswordChanged(String passwordStr) = _$_PasswordChanged;

  String get passwordStr;
  @JsonKey(ignore: true)
  _$PasswordChangedCopyWith<_PasswordChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ToggleShowPasswordPressedCopyWith<$Res> {
  factory _$ToggleShowPasswordPressedCopyWith(_ToggleShowPasswordPressed value,
          $Res Function(_ToggleShowPasswordPressed) then) =
      __$ToggleShowPasswordPressedCopyWithImpl<$Res>;
}

/// @nodoc
class __$ToggleShowPasswordPressedCopyWithImpl<$Res>
    extends _$RegisterEventCopyWithImpl<$Res>
    implements _$ToggleShowPasswordPressedCopyWith<$Res> {
  __$ToggleShowPasswordPressedCopyWithImpl(_ToggleShowPasswordPressed _value,
      $Res Function(_ToggleShowPasswordPressed) _then)
      : super(_value, (v) => _then(v as _ToggleShowPasswordPressed));

  @override
  _ToggleShowPasswordPressed get _value =>
      super._value as _ToggleShowPasswordPressed;
}

/// @nodoc

class _$_ToggleShowPasswordPressed implements _ToggleShowPasswordPressed {
  const _$_ToggleShowPasswordPressed();

  @override
  String toString() {
    return 'RegisterEvent.toggleShowPasswordPressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ToggleShowPasswordPressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return toggleShowPasswordPressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return toggleShowPasswordPressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (toggleShowPasswordPressed != null) {
      return toggleShowPasswordPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return toggleShowPasswordPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return toggleShowPasswordPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (toggleShowPasswordPressed != null) {
      return toggleShowPasswordPressed(this);
    }
    return orElse();
  }
}

abstract class _ToggleShowPasswordPressed implements RegisterEvent {
  const factory _ToggleShowPasswordPressed() = _$_ToggleShowPasswordPressed;
}

/// @nodoc
abstract class _$RegisterPressedCopyWith<$Res> {
  factory _$RegisterPressedCopyWith(
          _RegisterPressed value, $Res Function(_RegisterPressed) then) =
      __$RegisterPressedCopyWithImpl<$Res>;
}

/// @nodoc
class __$RegisterPressedCopyWithImpl<$Res>
    extends _$RegisterEventCopyWithImpl<$Res>
    implements _$RegisterPressedCopyWith<$Res> {
  __$RegisterPressedCopyWithImpl(
      _RegisterPressed _value, $Res Function(_RegisterPressed) _then)
      : super(_value, (v) => _then(v as _RegisterPressed));

  @override
  _RegisterPressed get _value => super._value as _RegisterPressed;
}

/// @nodoc

class _$_RegisterPressed implements _RegisterPressed {
  const _$_RegisterPressed();

  @override
  String toString() {
    return 'RegisterEvent.registerPressed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _RegisterPressed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return registerPressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return registerPressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (registerPressed != null) {
      return registerPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return registerPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return registerPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (registerPressed != null) {
      return registerPressed(this);
    }
    return orElse();
  }
}

abstract class _RegisterPressed implements RegisterEvent {
  const factory _RegisterPressed() = _$_RegisterPressed;
}

/// @nodoc
abstract class _$ResetErrorCopyWith<$Res> {
  factory _$ResetErrorCopyWith(
          _ResetError value, $Res Function(_ResetError) then) =
      __$ResetErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$ResetErrorCopyWithImpl<$Res> extends _$RegisterEventCopyWithImpl<$Res>
    implements _$ResetErrorCopyWith<$Res> {
  __$ResetErrorCopyWithImpl(
      _ResetError _value, $Res Function(_ResetError) _then)
      : super(_value, (v) => _then(v as _ResetError));

  @override
  _ResetError get _value => super._value as _ResetError;
}

/// @nodoc

class _$_ResetError implements _ResetError {
  const _$_ResetError();

  @override
  String toString() {
    return 'RegisterEvent.resetError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _ResetError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return resetError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return resetError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (resetError != null) {
      return resetError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return resetError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return resetError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (resetError != null) {
      return resetError(this);
    }
    return orElse();
  }
}

abstract class _ResetError implements RegisterEvent {
  const factory _ResetError() = _$_ResetError;
}

/// @nodoc
abstract class _$ResetAllCopyWith<$Res> {
  factory _$ResetAllCopyWith(_ResetAll value, $Res Function(_ResetAll) then) =
      __$ResetAllCopyWithImpl<$Res>;
}

/// @nodoc
class __$ResetAllCopyWithImpl<$Res> extends _$RegisterEventCopyWithImpl<$Res>
    implements _$ResetAllCopyWith<$Res> {
  __$ResetAllCopyWithImpl(_ResetAll _value, $Res Function(_ResetAll) _then)
      : super(_value, (v) => _then(v as _ResetAll));

  @override
  _ResetAll get _value => super._value as _ResetAll;
}

/// @nodoc

class _$_ResetAll implements _ResetAll {
  const _$_ResetAll();

  @override
  String toString() {
    return 'RegisterEvent.resetAll()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _ResetAll);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String userNameStr) userNameChanged,
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function() toggleShowPasswordPressed,
    required TResult Function() registerPressed,
    required TResult Function() resetError,
    required TResult Function() resetAll,
  }) {
    return resetAll();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
  }) {
    return resetAll?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String userNameStr)? userNameChanged,
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function()? toggleShowPasswordPressed,
    TResult Function()? registerPressed,
    TResult Function()? resetError,
    TResult Function()? resetAll,
    required TResult orElse(),
  }) {
    if (resetAll != null) {
      return resetAll();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNameChanged value) userNameChanged,
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_ToggleShowPasswordPressed value)
        toggleShowPasswordPressed,
    required TResult Function(_RegisterPressed value) registerPressed,
    required TResult Function(_ResetError value) resetError,
    required TResult Function(_ResetAll value) resetAll,
  }) {
    return resetAll(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
  }) {
    return resetAll?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNameChanged value)? userNameChanged,
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_ToggleShowPasswordPressed value)?
        toggleShowPasswordPressed,
    TResult Function(_RegisterPressed value)? registerPressed,
    TResult Function(_ResetError value)? resetError,
    TResult Function(_ResetAll value)? resetAll,
    required TResult orElse(),
  }) {
    if (resetAll != null) {
      return resetAll(this);
    }
    return orElse();
  }
}

abstract class _ResetAll implements RegisterEvent {
  const factory _ResetAll() = _$_ResetAll;
}

/// @nodoc
class _$RegisterStateTearOff {
  const _$RegisterStateTearOff();

  _RegisterState call(
      {required UserName userName,
      required EmailAddress emailAddress,
      required Password password,
      required bool showPassword,
      required bool showErrorMessages,
      required bool isSubmitting,
      required Option<Either<AuthFailure, User>> authFailureOrSuccessOption}) {
    return _RegisterState(
      userName: userName,
      emailAddress: emailAddress,
      password: password,
      showPassword: showPassword,
      showErrorMessages: showErrorMessages,
      isSubmitting: isSubmitting,
      authFailureOrSuccessOption: authFailureOrSuccessOption,
    );
  }
}

/// @nodoc
const $RegisterState = _$RegisterStateTearOff();

/// @nodoc
mixin _$RegisterState {
  UserName get userName => throw _privateConstructorUsedError;
  EmailAddress get emailAddress => throw _privateConstructorUsedError;
  Password get password => throw _privateConstructorUsedError;
  bool get showPassword => throw _privateConstructorUsedError;
  bool get showErrorMessages => throw _privateConstructorUsedError;
  bool get isSubmitting => throw _privateConstructorUsedError;
  Option<Either<AuthFailure, User>> get authFailureOrSuccessOption =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RegisterStateCopyWith<RegisterState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterStateCopyWith<$Res> {
  factory $RegisterStateCopyWith(
          RegisterState value, $Res Function(RegisterState) then) =
      _$RegisterStateCopyWithImpl<$Res>;
  $Res call(
      {UserName userName,
      EmailAddress emailAddress,
      Password password,
      bool showPassword,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<AuthFailure, User>> authFailureOrSuccessOption});
}

/// @nodoc
class _$RegisterStateCopyWithImpl<$Res>
    implements $RegisterStateCopyWith<$Res> {
  _$RegisterStateCopyWithImpl(this._value, this._then);

  final RegisterState _value;
  // ignore: unused_field
  final $Res Function(RegisterState) _then;

  @override
  $Res call({
    Object? userName = freezed,
    Object? emailAddress = freezed,
    Object? password = freezed,
    Object? showPassword = freezed,
    Object? showErrorMessages = freezed,
    Object? isSubmitting = freezed,
    Object? authFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as UserName,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as Password,
      showPassword: showPassword == freezed
          ? _value.showPassword
          : showPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as bool,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<AuthFailure, User>>,
    ));
  }
}

/// @nodoc
abstract class _$RegisterStateCopyWith<$Res>
    implements $RegisterStateCopyWith<$Res> {
  factory _$RegisterStateCopyWith(
          _RegisterState value, $Res Function(_RegisterState) then) =
      __$RegisterStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {UserName userName,
      EmailAddress emailAddress,
      Password password,
      bool showPassword,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<AuthFailure, User>> authFailureOrSuccessOption});
}

/// @nodoc
class __$RegisterStateCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res>
    implements _$RegisterStateCopyWith<$Res> {
  __$RegisterStateCopyWithImpl(
      _RegisterState _value, $Res Function(_RegisterState) _then)
      : super(_value, (v) => _then(v as _RegisterState));

  @override
  _RegisterState get _value => super._value as _RegisterState;

  @override
  $Res call({
    Object? userName = freezed,
    Object? emailAddress = freezed,
    Object? password = freezed,
    Object? showPassword = freezed,
    Object? showErrorMessages = freezed,
    Object? isSubmitting = freezed,
    Object? authFailureOrSuccessOption = freezed,
  }) {
    return _then(_RegisterState(
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as UserName,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as Password,
      showPassword: showPassword == freezed
          ? _value.showPassword
          : showPassword // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as bool,
      isSubmitting: isSubmitting == freezed
          ? _value.isSubmitting
          : isSubmitting // ignore: cast_nullable_to_non_nullable
              as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<AuthFailure, User>>,
    ));
  }
}

/// @nodoc

class _$_RegisterState implements _RegisterState {
  const _$_RegisterState(
      {required this.userName,
      required this.emailAddress,
      required this.password,
      required this.showPassword,
      required this.showErrorMessages,
      required this.isSubmitting,
      required this.authFailureOrSuccessOption});

  @override
  final UserName userName;
  @override
  final EmailAddress emailAddress;
  @override
  final Password password;
  @override
  final bool showPassword;
  @override
  final bool showErrorMessages;
  @override
  final bool isSubmitting;
  @override
  final Option<Either<AuthFailure, User>> authFailureOrSuccessOption;

  @override
  String toString() {
    return 'RegisterState(userName: $userName, emailAddress: $emailAddress, password: $password, showPassword: $showPassword, showErrorMessages: $showErrorMessages, isSubmitting: $isSubmitting, authFailureOrSuccessOption: $authFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _RegisterState &&
            const DeepCollectionEquality().equals(other.userName, userName) &&
            const DeepCollectionEquality()
                .equals(other.emailAddress, emailAddress) &&
            const DeepCollectionEquality().equals(other.password, password) &&
            const DeepCollectionEquality()
                .equals(other.showPassword, showPassword) &&
            const DeepCollectionEquality()
                .equals(other.showErrorMessages, showErrorMessages) &&
            const DeepCollectionEquality()
                .equals(other.isSubmitting, isSubmitting) &&
            const DeepCollectionEquality().equals(
                other.authFailureOrSuccessOption, authFailureOrSuccessOption));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(userName),
      const DeepCollectionEquality().hash(emailAddress),
      const DeepCollectionEquality().hash(password),
      const DeepCollectionEquality().hash(showPassword),
      const DeepCollectionEquality().hash(showErrorMessages),
      const DeepCollectionEquality().hash(isSubmitting),
      const DeepCollectionEquality().hash(authFailureOrSuccessOption));

  @JsonKey(ignore: true)
  @override
  _$RegisterStateCopyWith<_RegisterState> get copyWith =>
      __$RegisterStateCopyWithImpl<_RegisterState>(this, _$identity);
}

abstract class _RegisterState implements RegisterState {
  const factory _RegisterState(
      {required UserName userName,
      required EmailAddress emailAddress,
      required Password password,
      required bool showPassword,
      required bool showErrorMessages,
      required bool isSubmitting,
      required Option<Either<AuthFailure, User>>
          authFailureOrSuccessOption}) = _$_RegisterState;

  @override
  UserName get userName;
  @override
  EmailAddress get emailAddress;
  @override
  Password get password;
  @override
  bool get showPassword;
  @override
  bool get showErrorMessages;
  @override
  bool get isSubmitting;
  @override
  Option<Either<AuthFailure, User>> get authFailureOrSuccessOption;
  @override
  @JsonKey(ignore: true)
  _$RegisterStateCopyWith<_RegisterState> get copyWith =>
      throw _privateConstructorUsedError;
}
