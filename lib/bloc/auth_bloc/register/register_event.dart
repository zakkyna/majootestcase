part of 'register_bloc.dart';

@freezed
class RegisterEvent with _$RegisterEvent {
  const factory RegisterEvent.userNameChanged(String userNameStr) =
      _UserNameChanged;
  const factory RegisterEvent.emailChanged(String emailStr) = _EmailChanged;
  const factory RegisterEvent.passwordChanged(String passwordStr) =
      _PasswordChanged;
  const factory RegisterEvent.toggleShowPasswordPressed() =
      _ToggleShowPasswordPressed;
  const factory RegisterEvent.registerPressed() = _RegisterPressed;
  const factory RegisterEvent.resetError() = _ResetError;
  const factory RegisterEvent.resetAll() = _ResetAll;
}
