import 'package:bloc/bloc.dart';
import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/auth/auth_value_objects.dart';
import 'package:majootestcase/models/failures/auth/auth_failure.dart';
import 'package:majootestcase/models/user/user.dart';
import 'package:majootestcase/repository/auth/auth_repository.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

@lazySingleton
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository _authRepository;

  LoginBloc(this._authRepository) : super(LoginState.initial()) {
    on<LoginEvent>((event, emit) async {
      await event.map(
        emailChanged: (e) async {
          emit(
            state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        passwordChanged: (e) async {
          emit(
            state.copyWith(
              password: Password(e.passwordStr),
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        toggleShowPasswordPressed: (e) async {
          emit(
            state.copyWith(
              showPassword: !state.showPassword,
              authFailureOrSuccessOption: none(),
            ),
          );
        },
        loginPressed: (e) async {
          Either<AuthFailure, User>? failureOrSuccess;

          final isEmailValid = state.emailAddress.isValid();
          final isPasswordValid = state.password.isValid();
          if (isEmailValid && isPasswordValid) {
            emit(
              state.copyWith(
                isSubmitting: true,
                authFailureOrSuccessOption: none(),
              ),
            );

            failureOrSuccess = await _authRepository.signInWithEmailAndPassword(
              emailAddress: state.emailAddress,
              password: state.password,
            );
          }
          emit(
            state.copyWith(
              isSubmitting: false,
              showErrorMessages: true,
              authFailureOrSuccessOption: optionOf(failureOrSuccess),
            ),
          );
        },
        resetError: (_event) async {
          emit(
            state.copyWith(
              showErrorMessages: false,
            ),
          );
        },
        resetAll: (_event) async {
          emit(LoginState.initial());
        },
      );
    });
  }
}
