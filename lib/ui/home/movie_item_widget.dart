import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/common/widget/spacing.dart';
import 'package:majootestcase/models/movie/movie.dart';
import 'package:majootestcase/ui/movie/movie_detail_page.dart';

class MovieItemWidget extends StatelessWidget {
  final Movie movie;
  const MovieItemWidget(this.movie, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => MovieDetailPage(movie)));
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: [
            Hero(
              tag: 'movie_${movie.id}',
              child: CachedNetworkImage(
                imageUrl: 'https://image.tmdb.org/t/p/w500/${movie.posterPath}',
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.black.withOpacity(0.5),
                child: Hero(
                  tag: 'movie_${movie.id}_title',
                  child: Text(
                    movie.title.isEmpty ? movie.name : movie.title,
                    style: Styles.lato14Bold.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
