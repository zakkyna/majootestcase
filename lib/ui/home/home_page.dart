import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_scafold.dart';
import 'package:majootestcase/common/widget/custom_simple_dialog.dart';
import 'package:majootestcase/common/widget/spacing.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home/movie_item_widget.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homeBloc = context.read<HomeBloc>();
    return CustomScafold(
      title: 'Home',
      suffixWidget: IconButton(
        icon: Icon(
          Icons.power_settings_new_rounded,
          color: Colors.white,
          size: 30,
        ),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => CustomSimpleDialog(
              context,
              onBackPressed: () async {
                context.read<AuthBloc>().add(AuthEvent.signOut());
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ),
                    (route) => false);
              },
              title: 'Log Out',
              description: 'Anda yakin ingin keluar?',
              buttonText: 'Keluar',
            ),
          );
        },
      ),
      body: BlocBuilder<HomeBloc, HomeState>(
        bloc: _homeBloc..add(HomeEvent.getMovies()),
        builder: (context, state) => state.moviesFailureOrSuccessOption.match(
          (failureOrSuccess) => failureOrSuccess.match(
            (failure) {
              final stringError = failure.map(
                noInternet: (_) => 'Tidak ada koneksi internet',
                timeOut: (_) => 'Timeout',
                serverError: (_) => 'Server error',
                unexpected: (_) => 'Terjadi kesalahan',
                failedParseJson: (_) => 'Terjadi kesalahan data',
              );
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    stringError,
                    style: Styles.lato14Regular,
                  ),
                  AddVerticalSpace(20.h),
                  CustomButton(
                    margin: EdgeInsets.all(20.w),
                    onPressed: () {
                      _homeBloc.add(HomeEvent.getMovies());
                    },
                    label: 'Refresh',
                  )
                ],
              );
            },
            (success) => ListView(
              children: [
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    'Trending Movies',
                    style: Styles.lato18Bold,
                  ),
                ),
                GridView.count(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  crossAxisCount: 2,
                  shrinkWrap: true,
                  childAspectRatio: 2 / 3,
                  mainAxisSpacing: 20,
                  crossAxisSpacing: 20,
                  physics: NeverScrollableScrollPhysics(),
                  children: success.results.map((movie) {
                    return MovieItemWidget(movie);
                  }).toList(),
                ),
              ],
            ),
          ),
          () => LoadingIndicator(),
        ),
      ),
    );
  }
}
