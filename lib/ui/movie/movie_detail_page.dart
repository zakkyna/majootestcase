import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/common/widget/spacing.dart';
import 'package:majootestcase/models/movie/movie.dart';

class MovieDetailPage extends StatelessWidget {
  final Movie movie;
  const MovieDetailPage(this.movie, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Stack(
            children: [
              Hero(
                tag: 'movie_${movie.id}',
                child: SizedBox(
                  width: double.infinity,
                  child: CachedNetworkImage(
                    imageUrl:
                        'https://image.tmdb.org/t/p/w500/${movie.posterPath}',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  elevation: 2.0,
                  fillColor: Colors.black54,
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 28.0,
                  ),
                  padding: EdgeInsets.all(15.0),
                  shape: CircleBorder(),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: EdgeInsets.all(20),
                  color: Colors.black.withOpacity(0.5),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Ratings : ${movie.voteAverage}',
                              style: Styles.lato14Medium.copyWith(
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              'Popularity : ${movie.popularity}',
                              style: Styles.lato14Medium.copyWith(
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Text(
                          'Release Date : ${movie.releaseDate}',
                          style: Styles.lato14Medium.copyWith(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          AddVerticalSpace(20),
          Hero(
              tag: 'movie_${movie.id}_title',
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  movie.title.isEmpty ? movie.name : movie.title,
                  style: Styles.lato16Bold,
                ),
              )),
          AddVerticalSpace(20),
          Hero(
            tag: 'movie_${movie.id}_overview',
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                movie.overview,
                style: Styles.lato14Regular,
              ),
            ),
          ),
          AddVerticalSpace(40)
        ],
      ),
    );
  }
}
