import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/login/login_bloc.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_scafold.dart';
import 'package:majootestcase/common/widget/custom_simple_dialog.dart';
import 'package:majootestcase/common/widget/custom_textfield.dart';
import 'package:majootestcase/common/widget/spacing.dart';
import 'package:majootestcase/injection.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  late final _loginBloc = context.read<LoginBloc>();
  late final _authBloc = getIt<AuthBloc>();

  @override
  void initState() {
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    super.initState();
  }

  void _showDialogError(
    BuildContext context,
    String description,
  ) {
    showDialog(
      context: context,
      builder: (context) => CustomSimpleDialog(
        context,
        title: 'Maaf',
        description: description,
      ),
    );
  }

  void _onEmailChanged() {
    _loginBloc.add(
      LoginEvent.emailChanged(_emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      LoginEvent.passwordChanged(_passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _loginBloc.add(
      const LoginEvent.loginPressed(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScafold(
      title: 'Movie Majoo',
      body: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) {
          state.authFailureOrSuccessOption.match(
            (either) => either.fold(
              (failure) {
                _showDialogError(context, failure.mapErrorMessage());
              },
              (_) async {
                _authBloc.add(const AuthEvent.authCheck());
                _loginBloc.add(const LoginEvent.resetAll());
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => const HomePage()),
                    (route) => false);
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const <Widget>[
                          Flexible(
                            child: Text('Login Berhasil !'),
                          ),
                          Icon(
                            Icons.check_circle_outline,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      backgroundColor: Colors.green,
                    ),
                  );
              },
            ),
            () {},
          );
        },
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  color: AppColors.mainColor,
                  height: MediaQuery.of(context).padding.top,
                ),
                Expanded(
                  child: ListView(
                    padding: const EdgeInsets.symmetric(
                        horizontal: Dimens.defaultMargin),
                    children: <Widget>[
                      AddVerticalSpace(20.h),
                      Text(
                        'Masuk',
                        style: Styles.lato20Bold,
                        textAlign: TextAlign.left,
                      ),
                      AddVerticalSpace(20.h),
                      Text(
                        'Email',
                        style: Styles.textFieldLabel,
                      ),
                      AddVerticalSpace(12.h),
                      CustomTextField(
                        prefix: Icon(
                          Icons.email,
                          color: AppColors.mainColor,
                        ),
                        hintText: 'Email terdaftar',
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        autovalidateMode: state.showErrorMessages
                            ? AutovalidateMode.always
                            : AutovalidateMode.disabled,
                        validator: (_) => state.emailAddress.value.match(
                          (f) => f.maybeMap(
                            invalidEmail: (_) => 'Masukan Email yang valid',
                            empty: (_) => 'Email tidak boleh kosong',
                            orElse: () => null,
                          ),
                          (_) => null,
                        ),
                      ),
                      AddVerticalSpace(20.h),
                      Text(
                        'Password',
                        style: Styles.textFieldLabel,
                      ),
                      AddVerticalSpace(12.h),
                      CustomTextField(
                        prefix: Icon(
                          Icons.lock,
                          color: AppColors.mainColor,
                        ),
                        hintText: 'Password terdaftar',
                        keyboardType: TextInputType.visiblePassword,
                        controller: _passwordController,
                        obscureText: !state.showPassword,
                        autovalidateMode: state.showErrorMessages
                            ? AutovalidateMode.always
                            : AutovalidateMode.disabled,
                        validator: (_) => state.password.value.fold(
                          (f) => f.maybeMap(
                            empty: (_) => 'Password tidak boleh kosong',
                            orElse: () => null,
                          ),
                          (_) => null,
                        ),
                        maxLines: 1,
                        suffixIcon: GestureDetector(
                          onTap: () => _loginBloc.add(
                              const LoginEvent.toggleShowPasswordPressed()),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 15),
                            child: Icon(
                              state.showPassword
                                  ? Icons.visibility_rounded
                                  : Icons.visibility_off_rounded,
                              color: AppColors.mainColor,
                              size: 28,
                            ),
                          ),
                        ),
                      ),
                      AddVerticalSpace(40.h),
                      CustomButton(
                        label: 'Masuk',
                        child: state.isSubmitting
                            ? const CircularProgressIndicator()
                            : null,
                        onPressed: state.isSubmitting ? null : _onFormSubmitted,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 34.h,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const RegisterPage(),
                                  ),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  'Daftar Akun',
                                  style: Styles.linkStyle,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
