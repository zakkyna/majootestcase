// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:hive/hive.dart' as _i3;
import 'package:injectable/injectable.dart' as _i2;

import 'bloc/auth_bloc/auth_bloc.dart' as _i12;
import 'bloc/auth_bloc/login/login_bloc.dart' as _i9;
import 'bloc/auth_bloc/register/register_bloc.dart' as _i11;
import 'bloc/home_bloc/home_bloc.dart' as _i13;
import 'repository/auth/auth_repository.dart' as _i7;
import 'repository/movie/movie_repository.dart' as _i10;
import 'services/implementation/local_storage.dart' as _i6;
import 'services/interfaces/i_local_database.dart' as _i4;
import 'services/interfaces/i_local_storage.dart' as _i5;
import 'services/interfaces/i_network_service.dart' as _i8;
import 'services/register_services.dart'
    as _i14; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final registerServices = _$RegisterServices();
  gh.lazySingleton<_i3.HiveInterface>(() => registerServices.hive);
  await gh.lazySingletonAsync<_i4.ILocalDatabase>(
      () => registerServices.localDatabase(),
      preResolve: true);
  gh.lazySingleton<_i5.ILocalStorage>(
      () => _i6.LocalStorage(get<_i3.HiveInterface>()));
  gh.factory<String>(() => registerServices.baseUrl, instanceName: 'baseUrl');
  gh.lazySingleton<_i7.AuthRepository>(() =>
      _i7.AuthRepository(get<_i4.ILocalDatabase>(), get<_i5.ILocalStorage>()));
  await gh.lazySingletonAsync<_i8.INetworkService>(
      () => registerServices.network(get<String>(instanceName: 'baseUrl')),
      preResolve: true);
  gh.lazySingleton<_i9.LoginBloc>(
      () => _i9.LoginBloc(get<_i7.AuthRepository>()));
  gh.lazySingleton<_i10.MovieRepository>(
      () => _i10.MovieRepository(get<_i8.INetworkService>()));
  gh.lazySingleton<_i11.RegisterBloc>(
      () => _i11.RegisterBloc(get<_i7.AuthRepository>()));
  gh.lazySingleton<_i12.AuthBloc>(
      () => _i12.AuthBloc(get<_i7.AuthRepository>()));
  gh.lazySingleton<_i13.HomeBloc>(
      () => _i13.HomeBloc(get<_i10.MovieRepository>()));
  return get;
}

class _$RegisterServices extends _i14.RegisterServices {}
