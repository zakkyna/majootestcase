import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/login/login_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/register/register_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/injection.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/utils/constant.dart';

const env = Environment.dev;

void main() async {
  await configureInjection(env);
  await Hive.initFlutter();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (context) => getIt<HomeBloc>(),
        ),
        BlocProvider<AuthBloc>(
          create: (context) => getIt<AuthBloc>(),
        ),
        BlocProvider<LoginBloc>(
          create: (context) => getIt<LoginBloc>(),
        ),
        BlocProvider<RegisterBloc>(
          create: (context) => getIt<RegisterBloc>(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(
          ScreenUtilConstants.width,
          ScreenUtilConstants.height,
        ),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: () => MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: AppColors.mainMaterialColor,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: MyHomePageScreen(),
        ),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      bloc: context.read<AuthBloc>()..add(AuthEvent.authCheck()),
      builder: (context, state) {
        return state.map(
          initial: (_) => LoadingIndicator(),
          loading: (_) => LoadingIndicator(),
          authenticated: (_) => HomePage(),
          unauthenticated: (_) => LoginPage(),
        );
      },
    );
  }
}
