import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/common/theme/theme.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_dialog.dart';
import 'package:majootestcase/common/widget/spacing.dart';

class CustomSimpleDialog extends StatelessWidget {
  final BuildContext context;
  final String title;
  final String description;
  final void Function()? onBackPressed;
  final String buttonText;
  const CustomSimpleDialog(
    this.context, {
    Key? key,
    required this.title,
    required this.description,
    this.onBackPressed,
    this.buttonText = 'Kembali',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      backgroundColor: Colors.white,
      borderRadius: BorderRadius.circular(10.r),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            title,
            style: Styles.lato20Bold,
            textAlign: TextAlign.center,
          ),
          AddVerticalSpace(40.h),
          Text(
            description,
            style: Styles.lato14Regular,
            textAlign: TextAlign.center,
          ),
          AddVerticalSpace(40.h),
          CustomButton(
              label: buttonText,
              onPressed: () {
                Navigator.of(this.context).pop();
                if (onBackPressed != null) {
                  onBackPressed!();
                }
              }),
        ],
      ),
    );
  }
}
