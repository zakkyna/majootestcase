part of 'theme.dart';

class Styles {
  Styles._();

  static TextStyle appBarTitleStyle = GoogleFonts.lato(
    color: Colors.white,
    fontSize: 20.sp,
    fontWeight: FontWeight.bold,
  );

  static TextStyle buttonLabelStyle = GoogleFonts.lato(
    color: Colors.white,
    fontSize: 14.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle inputStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle inputHintStyle = GoogleFonts.lato(
    color: AppColors.inputHintColor,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle inputErrorStyle = GoogleFonts.lato(
    color: Colors.red,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle linkStyle = GoogleFonts.lato(
    color: AppColors.mainColor,
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
  );

  static TextStyle textFieldLabel = GoogleFonts.lato(
    color: const Color(0xFF565656),
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle inputLabelStyle = GoogleFonts.lato(
    color: AppColors.mainColor,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle normalStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
  );

  static TextStyle smallStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle mediumStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );

  static TextStyle bottomNavTextStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
  );

  static ButtonStyle regularButton = ElevatedButton.styleFrom(
    onSurface: Colors.white,
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    primary: AppColors.mainColor,
    alignment: Alignment.center,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(Dimens.defaultBorderRadius),
    ),
    elevation: 0,
  );

  static ButtonStyle buttonOutlined = OutlinedButton.styleFrom(
    padding: const EdgeInsets.symmetric(horizontal: Dimens.defaultMargin),
    fixedSize: const Size(150, 40),
    alignment: Alignment.center,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(Dimens.defaultBorderRadius),
    ),
    side: const BorderSide(
      color: Color(0xffd0d0d0),
    ),
  );

  static TextStyle lato10Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato10Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato10Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato10SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato10Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato11Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 11.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato11Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 11.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato11Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 11.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato11SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 11.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato11Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 11.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato12Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato12Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato12Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato12SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato12Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato13Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 13.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato13Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 13.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato13Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 13.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato13SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 13.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato13Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 13.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato14Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato14Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato14Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato14SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato14Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato15Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 15.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato15Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 15.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato15Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 15.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato15SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 15.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato15Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 15.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato16Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 16.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato16Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 16.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato16Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 16.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato16SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato16Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 16.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato18Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato18Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato18Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato18SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato18Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato20Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 20.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato20Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 20.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato20Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 20.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato20SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 20.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato20Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 20.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato22Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 22.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato22Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 22.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato22Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 22.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato22SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 22.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato22Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 22.sp,
    fontWeight: FontWeight.w700,
  );

  static TextStyle lato24Light = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 24.sp,
    fontWeight: FontWeight.w300,
  );
  static TextStyle lato24Regular = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 24.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle lato24Medium = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 24.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle lato24SemiBold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 24.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle lato24Bold = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 24.sp,
    fontWeight: FontWeight.w700,
  );
}
