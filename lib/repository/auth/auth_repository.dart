import 'package:fpdart/fpdart.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/auth/auth_value_objects.dart';
import 'package:majootestcase/models/failures/auth/auth_failure.dart';
import 'package:majootestcase/models/user/user.dart';
import 'package:majootestcase/services/interfaces/i_local_database.dart';
import 'package:majootestcase/services/interfaces/i_local_storage.dart';

@lazySingleton
class AuthRepository {
  final ILocalDatabase _localDatabase;
  final ILocalStorage _localStorage;
  AuthRepository(this._localDatabase, this._localStorage);

  Future<Option<User>> getSignedInUser() async {
    final user = await _localStorage.getData('userStorage', key: 'userData');
    if (user == null) {
      return none();
    }

    return optionOf(User.fromJson(Map<String, dynamic>.from(user)));
  }

  Future<Either<AuthFailure, User>> signInWithEmailAndPassword({
    required EmailAddress emailAddress,
    required Password password,
  }) async {
    final emailValue = emailAddress.getOrCrash();
    final passwordValue = password.getOrCrash();
    final user = await _localDatabase.query(
      'user',
      where: 'email = ? and password = ?',
      whereArgs: [emailValue, passwordValue],
      limit: 1,
    );
    if (user.isEmpty) {
      return left(const AuthFailure.invalidEmailAndPasswordCombination());
    }
    await _localStorage.putDatum(
      'userStorage',
      key: 'userData',
      value: user.first,
    );
    return right(User.fromJson(user.first));
  }

  Future<Either<AuthFailure, User>> registerWithEmailAndPassword({
    required EmailAddress emailAddress,
    required Password password,
    required UserName userName,
  }) async {
    final emailValue = emailAddress.getOrCrash();
    final passwordValue = password.getOrCrash();
    final userNameValue = userName.getOrCrash();
    final user = await _localDatabase.query(
      'user',
      where: 'email = ?',
      whereArgs: [emailValue],
      limit: 1,
    );
    if (user.isNotEmpty) {
      return left(const AuthFailure.emailAlreadyInUse());
    }
    await _localDatabase.insert(
      'user',
      {
        'email': emailValue,
        'password': passwordValue,
        'username': userNameValue,
      },
    );
    await _localStorage.putDatum(
      'userStorage',
      key: 'userData',
      value: {
        'email': emailValue,
        'password': passwordValue,
        'username': userNameValue,
      },
    );
    return right(User(
      email: emailValue,
      password: passwordValue,
      userName: userNameValue,
    ));
  }

  Future<void> signOut() async {
    await _localStorage.delete('userStorage', key: 'userData');
  }
}
