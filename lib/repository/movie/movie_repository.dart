import 'package:flutter/foundation.dart';
import 'package:fpdart/fpdart.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/models/failures/movie/movie_failure.dart';
import 'package:majootestcase/models/movie/movie_response.dart';
import 'package:majootestcase/services/interfaces/i_network_service.dart';

@lazySingleton
class MovieRepository {
  final INetworkService _networkService;
  MovieRepository(this._networkService);

  Future<Either<MovieFailure, MovieResponse>> getMovies() async {
    try {
      final response = await _networkService.getHttp(path: '/trending/all/day');
      return response.match(
        (failure) {
          return failure.map(
            noInternet: (_) => left(MovieFailure.noInternet()),
            serverError: (response) => left(MovieFailure.serverError()),
            timeout: (_) => left(MovieFailure.timeOut()),
            other: (_) => left(MovieFailure.unexpected()),
          );
        },
        (success) {
          try {
            final movieResponse =
                MovieResponse.fromJson(Map<String, dynamic>.from(success));
            return right(movieResponse);
          } catch (e, stacktrace) {
            debugPrint(stacktrace.toString());
            return left(MovieFailure.failedParseJson());
          }
        },
      );
    } catch (e, stacktrace) {
      debugPrint(stacktrace.toString());

      return left(MovieFailure.unexpected());
    }
  }
}
