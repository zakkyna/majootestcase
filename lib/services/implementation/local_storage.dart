import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/services/interfaces/i_local_storage.dart';

@LazySingleton(as: ILocalStorage)
class LocalStorage implements ILocalStorage {
  final HiveInterface _hive;

  LocalStorage(this._hive);

  Future<LazyBox> openBox(
    String boxName,
  ) async {
    final List<int> hiveKey = await hiveKeys;

    final _openBox = await _hive.openLazyBox<dynamic>(
      boxName,
      encryptionCipher: HiveAesCipher(hiveKey),
    );
    debugPrint('====================Open Box=================');
    debugPrint('Database $boxName Open');
    debugPrint('=============================================');
    return _openBox;
  }

  Future<void> close(
    LazyBox _box,
  ) async {
    await _box.close();
    return;
  }

  @override
  Future<void> putData(
    String boxName, {
    required Map<dynamic, dynamic> data,
  }) async {
    final _box = await openBox(boxName);
    try {
      debugPrint('====================Put Data=================');
      debugPrint('$data');
      debugPrint('=============================================');
      await _box.putAll(data);
      debugPrint('================Put Data Success=============');
    } catch (e) {
      debugPrint('================Put Data Failed==============');
      debugPrint(e.toString());
      debugPrint('=============================================');
    }
    await close(_box);
    return;
  }

  @override
  Future<void> putDatum(
    String boxName, {
    required dynamic key,
    required dynamic value,
  }) async {
    final _box = await openBox(boxName);
    try {
      debugPrint('====================Put Data=================');
      debugPrint('$value');
      debugPrint('=============================================');
      await _box.put(key, value);
      debugPrint('================Put Data Success=============');
    } catch (e) {
      debugPrint('================Put Data Failed==============');
      debugPrint(e.toString());
      debugPrint('=============================================');
    }
    await close(_box);
    return;
  }

  @override
  Future<void> putListData(
    String boxName, {
    required List<dynamic> dataList,
  }) async {
    final _box = await openBox(boxName);
    try {
      debugPrint('====================Put Data=================');
      dataList.map((val) async {
        debugPrint('$val');
        await _box.add(val);
      });
      debugPrint('=============================================');
    } catch (e) {
      debugPrint('================Put Data Failed==============');
      debugPrint(e.toString());
      debugPrint('=============================================');
    }
    await close(_box);
    return;
  }

  @override
  Future<dynamic> getData(
    String boxName, {
    required dynamic key,
  }) async {
    final _box = await openBox(boxName);
    debugPrint('====================Get Data=================');
    final dynamic value = await _box.get(key);
    debugPrint('$value');
    debugPrint('=============================================');
    await close(_box);
    return value;
  }

  @override
  Future<List<dynamic>?> getListData(
    String boxName,
  ) async {
    final _box = await openBox(boxName);
    List<dynamic> data = <dynamic>[];
    for (int x = 0; x < _box.length; x++) {
      final dynamic item = await _box.getAt(x);
      if (item != null) {
        data.add(item);
      }
    }
    await close(_box);
    return data;
  }

  @override
  Future<void> clear(
    String boxName,
  ) async {
    final _box = await openBox(boxName);
    await _box.clear();
    await close(_box);
    return;
  }

  @override
  Future<void> delete(
    String boxName, {
    required String key,
  }) async {
    final _box = await openBox(boxName);
    await _box.delete(key);
    await close(_box);
    return;
  }

  @override
  Future<void> deleteList(
    String boxName, {
    required Iterable<dynamic> key,
  }) async {
    final _box = await openBox(boxName);
    await _box.deleteAll(key);
    await close(_box);
    return;
  }

  Future<List<int>> get hiveKeys async {
    const FlutterSecureStorage ss = FlutterSecureStorage();
    String? stringKey = await ss.read(key: 'boxKey');
    List<int> hiveKey;
    if (stringKey != null) {
      hiveKey = stringKey.codeUnits;
    } else {
      hiveKey = Hive.generateSecureKey();
      final Uint8List bytes = Uint8List.fromList(hiveKey);
      stringKey = String.fromCharCodes(bytes);
      await ss.write(key: 'boxKey', value: stringKey);
    }
    return hiveKey;
  }
}
