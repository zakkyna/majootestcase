import 'dart:async';
import 'package:majootestcase/services/interfaces/i_local_database.dart';
import 'package:sqflite/sqflite.dart' as sql;

class LocalDatabase implements ILocalDatabase {
  final FutureOr<void> Function(sql.Database, int)? onCreate;
  final String path;
  final int version;
  LocalDatabase({
    required this.onCreate,
    required this.path,
    required this.version,
  });
  //  Future<void> createTables(sql.Database database) async {
  //   await database.execute("""CREATE TABLE items(
  //       id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  //       title TEXT,
  //       description TEXT,
  //       createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
  //     )
  //     """);
  // }
// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  Future<sql.Database> open() async {
    return sql.openDatabase(
      path,
      version: version,
      onCreate: onCreate,
    );
  }

  Future<int> insert(String table, Map<String, dynamic> values) async {
    final db = await open();
    return await db.insert(table, values).then((value) async {
      await db.close();
      return value;
    });
  }

  Future<List<Map<String, dynamic>>> query(
    String table, {
    bool distinct = false,
    List<String>? columns,
    String? where,
    List<dynamic>? whereArgs,
    String? groupBy,
    String? having,
    String? orderBy,
    int? limit,
    int? offset,
  }) async {
    final db = await open();
    return await db
        .query(
      table,
      distinct: distinct,
      columns: columns,
      where: where,
      whereArgs: whereArgs,
      groupBy: groupBy,
      having: having,
      orderBy: orderBy,
      limit: limit,
      offset: offset,
    )
        .then((value) async {
      await db.close();
      return value;
    });
  }

  Future<int> update(
    String table,
    Map<String, dynamic> values, {
    String? where,
    List<dynamic>? whereArgs,
  }) async {
    final db = await open();
    return await db
        .update(
      table,
      values,
      where: where,
      whereArgs: whereArgs,
    )
        .then((value) async {
      await db.close();
      return value;
    });
  }

  Future<int> delete(
    String table, {
    String? where,
    List<dynamic>? whereArgs,
  }) async {
    final db = await open();
    return await db
        .delete(
      table,
      where: where,
      whereArgs: whereArgs,
    )
        .then((value) async {
      await db.close();
      return value;
    });
  }

  Future<int> deleteTable(String table) async {
    final db = await open();
    return await db.delete(table).then((value) async {
      await db.close();
      return value;
    });
  }
}
