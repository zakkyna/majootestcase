import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/services/implementation/local_database.dart';
import 'package:majootestcase/services/implementation/network_service.dart';
import 'package:majootestcase/services/interfaces/i_local_database.dart';
import 'package:majootestcase/services/interfaces/i_network_service.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@module
abstract class RegisterServices {
  @Named('baseUrl')
  String get baseUrl => 'https://api.themoviedb.org/3';

  @lazySingleton
  HiveInterface get hive => Hive;

  @preResolve
  @lazySingleton
  Future<ILocalDatabase> localDatabase() async {
    return LocalDatabase(
      onCreate: (database, version) {
        database.execute("""
            CREATE TABLE IF NOT EXISTS user(
              id INTEGER PRIMARY KEY,
              username TEXT,
              email TEXT,
              password TEXT)
            """);
      },
      path: 'majoo_111.db',
      version: 1,
    );
  }

  @preResolve
  @lazySingleton
  Future<INetworkService> network(
    @Named('baseUrl') String baseUrl,
  ) async {
    final interceptors = [
      PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 90)
    ];
    final queryParameters = {
      'api_key': '0bc9e6490f0a9aa230bd01e268411e10',
    };

    return NetworkService(
      baseUrl: baseUrl,
      interceptors: interceptors,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      queryParameters: queryParameters,
    );
  }
}
