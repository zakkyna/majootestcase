abstract class ILocalStorage {
  Future<void> putData(
    String boxName, {
    required Map<String, dynamic> data,
  });
  Future<void> putDatum(
    String boxName, {
    required String key,
    required dynamic value,
  });
  Future<void> putListData(
    String boxName, {
    required List<dynamic> dataList,
  });

  Future<dynamic> getData(
    String boxName, {
    required String key,
  });
  Future<List<dynamic>?> getListData(
    String boxName,
  );
  Future<void> clear(
    String boxName,
  );
  Future<void> delete(
    String boxName, {
    required String key,
  });
  Future<void> deleteList(
    String boxName, {
    required Iterable<dynamic> key,
  });
}
