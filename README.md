# majootestcase

A test flutter project

# How to run

flutter pub get

flutter pub run build_runner build --delete-conflicting-outputs

flutter run

# How to build

flutter pub get

flutter pub run build_runner build --delete-conflicting-outputs

flutter build apk

## Null Safety

This project have updated to null safety

## Flutter Version

This project have updated using Flutter version 2.10.0

## Architecture

I made little changes to the folder structure but made major changes to the concept of data flow architecture

Use Architecture DDD

![Architecture](https://resocoder.com/wp-content/uploads/2020/03/DDD-Flutter-Diagram-v3.svg)


# Use Library

[fpdart](https://pub.dev/packages/fpdart)

[BLoC](https://pub.dev/packages/flutter_bloc)

[freezed](https://pub.dev/packages/freezed)

[dio](https://pub.dev/packages/dio)

[get_it](https://pub.dev/packages/get_it)

[injectable](https://pub.dev/packages/injectable)

[sqflite](https://pub.dev/packages/sqflite)

[HiveDb](https://pub.dev/packages/hive)

